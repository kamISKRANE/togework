@extends('layouts.app')

@section('content')
<style>

</style>

<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<div class="col-9">
    <div class="container">
        <div class="row" style="margin-bottom:30px;">
            <div class="col-5">
                <h1>
                    <img src="{{ Avatar::create($user->email)->toGravatar() }}" style="max-width:50px;border-radius:50%;"/> {{$user->name }}
                </h1>
            </div>
            <div class="col-2" style="text-align:center;">
                <b>{{$user->followers()->count()}}</b><br> abonnés
            </div>
            <div class="col-2" style="text-align:center;">
                <b>{{$user->following()->count()}}</b><br> abonnements
            </div>
            <div class="col-3">
                @if(!$auth->isFollowing($user))
                <a class="btn btn-success" href="/follow/{{ $user->id }}">Suivre</a>
                @else
                <a class="btn btn-danger" href="/follow/{{ $user->id }}">Ne plus suivre</a>
                @endif
            </div>
        </div>
        <div>
            <h3>Posts de {{$user->name}}</h3>
            @foreach($user->posts as $post)
            <div class="post">
                <div>
                    <a href="{{ $post->author->getProfileLink() }}">{{ $post->author->name }}</a>
                    <small>{{ $post->timestamp() }}</small>
                </div>
                <p>{{ $post->post }}</p>
                <div>
                    @if(!$user->isLiking($post))
                    <a href="/like/{{ $post->id }}">J'aime</a>
                    @else
                    <a href="/like/{{ $post->id }}">Je n'aime plus</a>
                    @endif

                    ({{$post->getLikers()}})

                    @if($post->author->id == Auth::id())
                    -

                    <a href="/delete/{{ $post->id }}">delete</a>
                    @endif
                </div>
            </div>

            @endforeach
        </div>
    </div>
</div>

@endsection
