@foreach ($posts as $post)
<div class="post">
    <div style="font-size:19px;margin-bottom:10px;">
        <a href="{{ $post->author->getProfileLink() }}"><img src="{{ Avatar::create($post->author->name)->toBase64() }}" style="max-width: 25px;border-radius:50%;"/>  {{ $post->author->name }}</a>
        <small>{{ $post->timestamp() }}</small>
    </div>
    <p>{{ $post->post }}</p>
    <div>
        @if(!$user->isLiking($post))
        <a href="/like/{{ $post->id }}">J'aime</a>
        @else
        <a href="/like/{{ $post->id }}">Je n'aime plus</a>
        @endif

        ({{$post->getLikers()}})

        @if($post->author->id == Auth::id())
        -

        <a href="/delete/{{ $post->id }}">delete</a>
        @endif
    </div>
</div>
@endforeach
