@extends('layouts.app')

@section('title')
The Wall
@endsection

@section('content')

<div class="col-5">
    <div id="the-wall">
        <form class="post-form" method="POST" action="/write">
            @csrf
            <textarea class="textarea-post" name="post"></textarea>
            <div class="submit-post-div">
                <input class="btn btn-default" type="submit" value="Poster" />
            </div>
        </form>
    </div>
</div>

@endsection

@section('script')

<script type="text/javascript">
    var page = 0;
    var block = false;

    $(document).ready(function()
    {

        $(window).scroll(function() {
            if($(window).scrollTop() + $(window).height() >= $(document).height()-5 && !block) {
                page++;
                block = true;
                getData(page);
            }
        }).scroll();

    });

    function getData(page){
        $.ajax(
            {
                url: '?page=' + page,
                type: "get",
                datatype: "html"
            }).done(function(data){
                if(data){
                    block = false;
                    $("#the-wall").append(data);
                }
        }).fail(function(jqXHR, ajaxOptions, thrownError){
            alert('No response from server');
        });
    }
</script>
@endsection

