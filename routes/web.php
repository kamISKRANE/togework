<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'WallController@index')->name('wall');

Route::get('/delete/{id_post}', 'WallController@delete')->middleware('auth');

Route::get('/like/{id_post}', 'WallController@like')->middleware('auth');

Route::get('/wall-pagination-ajax','WallController@ajaxPagination')->name('wall.pagination');

Route::post('/write', 'WallController@write');

Route::get('/account', function(){
    return view('home');
})->name('home');

Route::get('/profile/{id_user?}', 'UserController@profile')->name('profile');

Route::get('/follow/{id_user}', 'UserController@follow');
