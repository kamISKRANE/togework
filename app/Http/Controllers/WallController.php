<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class WallController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $posts = Post::orderBy('created_at', 'desc')->paginate(6);
        $user = \Auth::user();

        if ($request->ajax()) {
            return view('wall.pagination-ajax', compact('posts', 'user'));
        }

        return view('wall.index', compact('posts', 'user'));
    }

    public function write(Request $request){
        $post = new Post();
        $post->post = $request->input('post');
        $post->user_id = \Auth::id();
        $post->save();

        toastr()->success('Votre post est arrivé!');

        return redirect()->route('wall');
    }

    public function delete(Request $request){
        $post = Post::find($request->id_post);

        if($post->user_id == \Auth::id())
        {
            $post->delete();

            $request->session()->flash('post', 'post deleted !');

            return redirect()->route('wall');
        }

        $request->session()->flash('post', 'Illegal operation !');

        toastr()->warning("Votre post s'est envolé..");

        return redirect()->route('wall');
    }

    public function like(Request $request){
        $user = \Auth::user();
        $post = Post::find($request->id_post);

        if(!$user->isLiking($post))
        {
            $user->like($post);
            toastr()->success("Vous avez du gout!");
        }else{
            $user->unlike($post);
            toastr()->warning("Vous avez changer d'humeur..");
        }

        return redirect()->route('wall');
    }
}
