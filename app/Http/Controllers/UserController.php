<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;



class UserController extends Controller
{
   	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

    }

    public function profile($userId = false){
    	//Recupre les infor de l'utilisateur
    	if($userId == false){
    		$user = Auth::user();
            toastr()->info("Que vous êtes beau <3");
    	}else{
	    	$user = User::find($userId);
    	}

        $auth = \Auth::user();


    	return view('profile', compact('user', 'auth'));

    }

    public function follow(Request $request){
        $auth = \Auth::user();
        $user = User::find($request->id_user);

        if(!$auth->isFollowing($user))
        {
            $auth->follow($user);
            toastr()->success("Vous êtes son nouveau fan !");
        }else{
            $auth->unfollow($user);
            toastr()->warning("Ne l'aimez vous donc plus ?");
        }

        return redirect()->route('profile', $user->id);
    }
}
