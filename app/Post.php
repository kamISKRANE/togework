<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Rennokki\Befriended\Traits\CanBeLiked;
use Rennokki\Befriended\Contracts\Likeable;

class Post extends Model implements Likeable
{
    use CanBeLiked;

    public function author()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function getLikers(){
        return $this->likers(User::class)->count();
    }

    public function timestamp()
    {

        $timestamp = $this->created_at;
        $now = Carbon::now();

        if($timestamp->diffInMinutes($now) < 60)
            return $timestamp->diffInMinutes($now).' min';

        if($timestamp->diffInHours($now) < 24)
            return $timestamp->diffInHours($now).' h';

        if(Carbon::yesterday() == $timestamp->format('Y-m-d 00:00:00'))
            return 'Hier, à '.$timestamp->format('H:m');

        if($timestamp->diffInYears($now) < 1)
            return $timestamp->format('d F, H:m');

        return $timestamp->format('d F');
    }
}
