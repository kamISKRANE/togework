<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Rennokki\Befriended\Traits\Like;
use Rennokki\Befriended\Contracts\Liking;
use Rennokki\Befriended\Traits\Follow;
use Rennokki\Befriended\Contracts\Following;

class User extends Authenticatable implements Liking, Following
{
    use Follow;
    use Notifiable;
    use Like;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getProfileLink(){
        if($this->id == \Auth::user()->id){
            return "/profile";
        }
        return "/profile/".$this->id;
    }

    public function posts()
    {
        return $this->hasMany(Post::class); // also Post belongsToMany Categories
    }
}
